# Configuration

The plumber API template is configurable via files 'config.yml' and
'secret/auth.yml' using the config package for R.

---

> You can specify which configuration is currently active by setting the
> `R_CONFIG_ACTIVE` environment variable. The `R_CONFIG_ACTIVE` variable is
> typically set within a site-wide `Renviron` or `Rprofile` (see [R Startup](https://stat.ethz.ch/R-manual/R-devel/library/base/html/Startup.html)
> for details on these files).
> 
> [...]
> 
> ## Defaults and Inheritance
> 
> The `default` configuration provides a set of values to use when no named
> configuration is active. Other configurations automatically inherit all
> `default` values so need only define values specialized for that
> configuration.

— [GitHub - rstudio/config](https://github.com/rstudio/config)

---

As the `default` configuration is used, there will be no further mention of
`default` when referencing to keys.

## Authentication

Ask your client to generate an `API key` and calculate a _salted_ `hash` of it:

1.  Generate `API Key`:  
    ```bash
    LC_ALL=C tr -cd 'A-Za-z0-9' < /dev/urandom | fold -w 32 | head -n 1
    ```
1.  Generate random `salt`:  
    ```bash
    LC_ALL=C tr -cd 'A-Za-z0-9' < /dev/urandom | fold -w 12 | head -n 1
    ```
1.  Calculate _salted_ hash of `API key`:  
    ```bash
    LC_ALL=C echo -n "API Key" | openssl dgst -sha256 -hmac "salt"
    ```

To enable authentication

1.  set `auth:enable` to `true` in file 'config.yml'
1.  ask your client for the `api_key:salt` and `hash`
1.  fill in the values in file 'secret/auth.yml' accordingly

---

> API key-based authentication is only considered secure if used together with
> other security mechanisms such as HTTPS/SSL.

— [OpenAPI Specification > Authentication > API Keys](https://swagger.io/docs/specification/authentication/api-keys/)

---

## Log level

Set the log level with key `log:level`.  
:information_source: See [init.R](init.R) for the list of distinct syslog
severity levels.

## OpenAPI spec

Plumber does a fairly good job automagically creating the OpenAPI specification.
Set the `openapi:spec:info` object keys to provide metadata about the API.  
:information_source: According to the
[OpenAPI specification](http://spec.openapis.org/oas/v3.0.3#info-object), fields
`title` and `version` are **REQUIRED**.

Set the `openapi:spec:external_docs` object keys `description` and `url` to
provide a link to the repository containing the source code and further
documentation.

With authentication enabled, both the security scheme object and security
requirement object is automatically added to the OpenAPI specification.

See function `custom_api_spec` in [functions.R](functions.R) for more
information.

## Plumber

The default serializer (`plumber::serializer_json()`) has been overridden by
setting `plumber:serializer` to
`!expr plumber::serializer_unboxed_json(na = "null")`.

The default host (`127.0.0.1`) has been overridden by setting `plumber:host` to
`0.0.0.0`.  
:information_source: `0.0.0.0` represents all IPv4 addresses and is required for
Docker/Kubernetes deployments.

Adjust `plumber:port` to whatever port you like to expose the plumber API.  
:information_source: Keep `port` 8000 (default) For Docker/Kubernetes
deployments.

## Swagger UI

Disable by setting `swagger_ui` to `false`.

# Documentation

The documentation focuses on those functions that define fundamental features of
this template:

1.  Optional API key authentication ('auth.yml')
1.  Reroute request based on query string (`api-version`)
1.  Return selected API version in response headers (`X-API-Version`)

## Routers

### `mount_version_router`

Each subfolder of 'versions' is mounted as a sub-router of the main router. The
sub-routers are named according to the names of the subfolders.

### `mount_sub_router`

Each file in these subfolders is mounted as a sub-router of the _version_
sub-routers. These sub-routers are named according to the file names (without
extension `.R`).

Exception: Handlers defines in files 'plumber.R' of these subfolders are mounted
directly on the _version_ sub-routers.

## Filters

### `req_logger`

This filter logs some information about the incoming request. If present, the
IPs of HTTP request header `HTTP_X_FORWARDED_FOR` are logged, `REMOTE_ADDR`
otherwise.

### `check_auth`

With authentication enabled, this filter allows access with a valid API key
only. The API key token may be sent in the headers, the query string or as
cookie of the HTTP request.

By default, the token is identified by `X-API-Key` or `api_key`. You may change
the pattern for the (case insensitive) regex matching with key
`auth:api_key:token_name`. 

## Preroute hooks

### `reroute_by_version`

Essentially, this hook reroutes the HTTP request based on the `api-version`
submitted in the query string. The API version is selected as follows:

1.  For `api-version=develop` the development version.
1.  For `api-version=latest` the latest available date version.
1.  For `api-version=YYYY-MM-DD` the latest available version up and including
    that date.

Furthermore, `api-version` is set to the selected API version.

## Postroute hooks

### `set_version_header`

This hook ensures, that the selected API version is only returned in the HTTP
response headers (as `X-API-Version`) when status is 200 (`OK`).

# Extension

## HTTP reverse proxy

When using API key-based [authentication](#authentication), put a HTTP reverse
proxy (e.g. Træfik, Nginx) with TLS termination in front of plumber API.

## Versions

Extend your code continuously in version develop.

Create a **new date version** of the API for each change that
**breaks functionality** on the client side.

If an `.R` file remains unchanged between versions, create a symlink to the
latest available version of that file.
