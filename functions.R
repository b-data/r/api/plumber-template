# Check authentication setup
check_auth_setup <- function(auth = cfg$auth) {
  if (auth$enable && (is.null(auth$api_key$salt) ||
                      is.null(auth$api_key$hash))) {
    stop("Authentication enabled, but no X-API-Key salt and/or hash defined!",
      call. = FALSE)
  }
}

# Concatenate HTTP request information for log message
req_logger_message <- function(req) {
  if (hasName(req, "HTTP_X_FORWARDED_FOR")) {
    paste(req$REQUEST_METHOD, req$PATH_INFO, "-", req$HTTP_USER_AGENT, "@",
      req$HTTP_X_FORWARDED_FOR)
  } else {
    paste(req$REQUEST_METHOD, req$PATH_INFO, "-", req$HTTP_USER_AGENT, "@",
      req$REMOTE_ADDR)
  }
}

# Log message
req_logger <- function(message, severity, log_level = cfg$log$level) {
  if (severities[[severity]] <= severities[[log_level]]) {
    cat(format(Sys.time(), "%FT%T%z"), paste0(severity, ":"), message, "\n")
  }
}

# Get API key from either HTTP headers, query string or cookies
get_api_key <- function(req, api_key_token_name = cfg$auth$api_key$token_name) {
  api_key <- c()

  sel <- lapply(list(names(req$HEADERS), names(req$args), names(req$cookies)),
    grepl, pattern = api_key_token_name, ignore.case = TRUE)

  if (any(sel[[1]])) {
    api_key$loc <- "headers"
    api_key$val <- req$HEADERS[sel[[1]]]
  } else if (any(sel[[2]])) {
    api_key$loc <- "query string"
    api_key$val <- unlist(req$args[sel[[2]]])
  } else if (any(sel[[3]])) {
    api_key$loc <- "cookies"
    api_key$val <- unlist(req$cookies[sel[[3]]])
  } else {
    api_key$val <- NULL
  }

  api_key
}

# Get valid versions
v_val <- function(path = "versions", pat = cfg$plumber$router_pattern) {
  stringi::stri_subset(dir(path), regex = pat$main_include)
}

# Mount sub-router
mount_sub_router <- function(sub, ver, v) {
  tmp <- plumber::Plumber$new(file.path("versions", v, sub))
  ver$mount(stringi::stri_sub(sub, 1, -3), tmp)
}

# Add separate router for each version
mount_version_router <- function(v, pr, pat = cfg$plumber$router_pattern) {
  # Mount main router
  if (file.exists(file.path("versions", v, "plumber.R"))) {
    ver <- plumber::Plumber$new(file.path("versions", v, "plumber.R"))
  } else {
    ver <- plumber::Plumber$new()
  }

  # Mount sub-routers
  sub <- list.files(file.path("versions", v), pattern = "*.R$")
  sub <- sub[stringi::stri_detect_regex(sub, pat$sub_exclude, negate = TRUE)]
  lapply(sub, mount_sub_router, ver, v)

  pr$mount(v, ver)
}

# API version to date
v2d <- function(v = "latest", path = "versions",
                pat = cfg$plumber$latest_pattern) {
  ifelse(
    v == "latest",
    tail(v_val(path, pat)[v >= v_val(path, pat)], 1),
    tail(v_val(path)[v >= v_val(path)], 1)
  )
}

# Reroute request based on queried version
reroute_by_version <- function(pr) {
  pr$registerHook("preroute",
  function(req, res, pat = cfg$plumber$router_pattern) {
    v1 <- stringi::stri_extract_first_regex(req$PATH_INFO,
      paste0(pat$main_include, "|latest"))
    if (!is.na(v1)) {
      v_sel <- v2d(v1)
      if (length(v_sel) > 0) {
        req$PATH_INFO <- stringi::stri_replace_first_regex(req$PATH_INFO,
          paste0(pat$main_include, "|latest"), v_sel)
        # Set selected API version
        req$args$`api-version` <- v_sel
      }
    } else {
      if (!any(unlist(lapply(no_preroute, grepl, req$PATH_INFO)))) {
        v_req <- plumber:::parseQS(req$QUERY_STRING)$`api-version`
        v2 <- stringi::stri_extract_first_regex(v_req,
          paste0(pat$main_include, "|latest"))
        v_sel <-  ifelse(!is.null(v_req) && !is.na(v_req), v2d(v2), v2d())
        if (!is.na(v_sel)) {
          req$PATH_INFO <- paste0("/", v_sel, req$PATH_INFO)
          # Set selected API version
          req$args$`api-version` <- v_sel
        } else {
          req$PATH_INFO <- paste0("/", v2, req$PATH_INFO)
        }
      }
    }
  })
}

# Return selected API version in HTTP headers
set_api_version_header <- function(pr) {
  pr$registerHook("postroute", function(req, res) {
    if (res$status == 200) {
      res$setHeader("X-API-Version", req$args$`api-version`)
    }
  })
}

# Custom API specification
custom_api_spec <- function(cfg, file = NULL) {
  # Use latest API version
  v <- v2d()

  # Mount main router
  tmp <- tempfile()
  file.create(tmp)
  file.append(tmp, "plumber.R")
  if (file.exists(file.path("versions", v, "plumber.R"))) {
    file.append(tmp, file.path("versions", v, "plumber.R"))
  }
  pr <- plumber::plumb(tmp)

  # Mount sub-routers
  sub <- list.files(file.path("versions", v), pattern = "*.R$")
  sub <- sub[stringi::stri_detect_regex(sub,
    cfg$plumber$router_pattern$sub_exclude, negate = TRUE)]
  lapply(sub, mount_sub_router, pr, v)

  # Clean API specification
  api_spec <- pr$getApiSpec()
  api_spec <- rlist::list.clean(api_spec, recursive = TRUE,
    function(x) identical(x, "Default response."))
  api_spec <- rlist::list.clean(api_spec, recursive = TRUE,
    function(x) length(x) == 0L)

  # Add info object
  api_spec$info <- list(
    title = cfg$openapi$spec$info$title,
    version = cfg$openapi$spec$info$version,
    description = cfg$openapi$spec$info$description,
    contact = list(email = cfg$openapi$spec$info$contact),
    license = list(name = cfg$openapi$spec$info$license)
  )

  # Add external docs object
  if (!is.null(cfg$openapi$spec$external_docs$description) &&
      !is.null(cfg$openapi$spec$external_docs$url)) {
    api_spec$externalDocs <- list(
      description = cfg$openapi$spec$external_docs$description,
      url = cfg$openapi$spec$external_docs$url
    )
  }

  if (cfg$auth$enable) {
    # Add security scheme object
    api_spec$components$securitySchemes$ApiKeyAuth <- list(
      type = "apiKey",
      "in" = "header",
      name = "X-API-Key"
    )
    # Add security requirement object
    api_spec$security <- list(list("ApiKeyAuth" = list()))
  }

  if (!is.null(file)) {
    jsonlite::write_json(api_spec, file, null = "null", na = "null",
      pretty = TRUE, auto_unbox = TRUE)
  }

  api_spec
}

# Get latest available schema up and including api-version date
get_schema <- function(path, v) {
  jsonlite::fromJSON(file.path(paste0("./schemas", path),
      v2d(v, paste0("./schemas", path)), "schema.json"))
}
