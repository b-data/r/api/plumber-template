FROM glcr.b-data.ch/r/plumber:4.3.0

LABEL org.opencontainers.image.licenses="MIT" \
      org.opencontainers.image.source="https://gitlab.com/b-data/r/api/plumber-template" \
      org.opencontainers.image.vendor="b-data GmbH" \
      org.opencontainers.image.authors="Olivier Benz <olivier.benz@b-data.ch>"

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    zlib1g-dev \
  ## Clean up
  && rm -rf /tmp/* \
  && rm -rf /var/lib/apt/lists/*

COPY . .

RUN Rscript -e "renv::restore()"

## Configure container startup
ENTRYPOINT []
CMD ["Rscript", "main.R"]
