#* Retrieve response body schema
#* @param api-version:character The version of the API (YYYY-MM-DD)
#* @get /response/versions
#* @response 200 OK
#* @response 401 Unauthorized
#* @tag schema
function(req, res, `api-version` = "latest") {
  get_schema(req$PATH_INFO, `api-version`)
}

#* Retrieve response body schema
#* @param api-version:character The version of the API (YYYY-MM-DD)
#* @get /response/schema
#* @response 200 OK
#* @response 401 Unauthorized
#* @tag schema
function(req, res, `api-version` = "latest") {
  get_schema(req$PATH_INFO, `api-version`)
}
